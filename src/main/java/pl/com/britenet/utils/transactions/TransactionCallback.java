package pl.com.britenet.utils.transactions;

import javax.persistence.EntityManager;

@FunctionalInterface
public interface TransactionCallback<E> {
    E doInTransaction(EntityManager entityManager);
}
