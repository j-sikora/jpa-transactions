package pl.com.britenet.utils.transactions;

import com.mchange.v2.c3p0.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;

public class VerboseConnectionCustomizer implements ConnectionCustomizer
{
    public void onAcquire( Connection c, String pdsIdt )
    {
        System.err.println("Acquired " + c + " [" + pdsIdt + "], isolationLvl=" + getTransactionIsolationLevel(c));
    }

    public void onDestroy(Connection c, String pdsIdt) {
        System.err.println("Destroying " + c + " [" + pdsIdt + "], isolationLvl=" + getTransactionIsolationLevel(c));
    }

    public void onCheckOut(Connection c, String pdsIdt) {
        System.err.println("Checked out " + c + " [" + pdsIdt + "], isolationLvl=" + getTransactionIsolationLevel(c));
    }

    public void onCheckIn(Connection c, String pdsIdt) {
        System.err.println("Checking in " + c + " [" + pdsIdt + "], isolationLvl=" + getTransactionIsolationLevel(c));
    }

    private Optional<Integer> getTransactionIsolationLevel(Connection connection) {
        try {
            return Optional.of(connection.getTransactionIsolation());
        }
        catch (SQLException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }
}