package pl.com.britenet.utils.transactions;

import lombok.extern.log4j.Log4j2;
import org.hibernate.Session;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

@Log4j2
public class TransactionExecutor {
    private EntityManagerFactory entityManagerFactory;

    private TransactionExecutor(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public static TransactionExecutor create(EntityManagerFactory entityManagerFactory) {
        return new TransactionExecutor(entityManagerFactory);
    }

    private void handleException(EntityTransaction transaction, Exception e) {
        log.error("Transaction execution error:" + e.getMessage(), e);
        if(transaction.isActive()) {
            transaction.rollback();
        }
        throw new TransactionException(e);
    }

    public <T> T execute(TransactionCallback<T> callback, TransactionAttributes transactionAttributes) {
        T result = null;
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            processTransactionAttributes(entityManager, transactionAttributes);
            transaction.begin();
            result = callback.doInTransaction(entityManager);
            transaction.commit();
        } catch (Exception e) {
            handleException(transaction, e);
        }
        finally {
            entityManager.close();
        }
        return result;
    }

    public <T> T execute(TransactionCallback<T> callback) {
        return execute(callback, null);
    }

    public void executeWithoutResult(TransactionCallbackVoid callback, TransactionAttributes transactionAttributes) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            processTransactionAttributes(entityManager, transactionAttributes);
            transaction.begin();
            callback.doInTransaction(entityManager);
            transaction.commit();
        } catch (Exception e) {
            handleException(transaction, e);
        } finally {
            entityManager.close();
        }
    }

    public void executeWithoutResult(TransactionCallbackVoid callback) {
        executeWithoutResult(callback, null);
    }

     private void processTransactionAttributes(final EntityManager entityManager, final TransactionAttributes transactionAttributes) {
        if(transactionAttributes == null) {
            return;
        }
        transactionAttributes.getIsolationLevel().ifPresent(level ->
            setIsolationLevel(entityManager, level.getLevel())
        );
    }

    private void setIsolationLevel(final EntityManager entityManager, final int isolationLevel) {
        Session session = entityManager.unwrap(Session.class);
        session.doWork(connection -> {
            System.out.println("Changing isolation level: " + connection.getTransactionIsolation() + "=>" + isolationLevel + ", (connection:" + connection + ")");
            connection.setTransactionIsolation(isolationLevel);
            System.out.println("Isolation level changed: " + connection.getTransactionIsolation() + ", (connection:" + connection + ")");
        });
    }
}
