package pl.com.britenet.utils.transactions;

import javax.persistence.EntityManager;

@FunctionalInterface
public interface TransactionCallbackVoid {
    void doInTransaction(EntityManager entityManager);
}
