package pl.com.britenet.utils.transactions;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

import java.util.Optional;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class TransactionAttributes {
    private final TransactionIsolationLevel isolationLevel;

    public static TransactionAttributes create(TransactionIsolationLevel isolationLevel) {
        return new TransactionAttributes(isolationLevel);
    }

    public Optional<TransactionIsolationLevel> getIsolationLevel() {
        return Optional.ofNullable(isolationLevel);
    }
}
