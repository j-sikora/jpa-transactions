package pl.com.britenet.utils.transactions;

public class TransactionException extends RuntimeException {

    public TransactionException() {
    }

    public TransactionException(String s) {
        super(s);
    }

    public TransactionException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public TransactionException(Throwable throwable) {
        super(throwable);
    }

    public TransactionException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
