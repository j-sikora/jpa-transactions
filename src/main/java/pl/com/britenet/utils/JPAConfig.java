package pl.com.britenet.utils;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Map;

public class JPAConfig {
    private static final String PERSISTENCE_UNIT_NAME = "PERSISTENCE";
    private EntityManagerFactory factory;
    private final Map properties;

    public JPAConfig(Map properties) {
        this.properties = properties;
    }

    public JPAConfig() {
        this(null);
    }

    public EntityManagerFactory getEntityManagerFactory() {
        if (factory == null) {
            factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME, properties);
        }
        return factory;
    }

    public void shutdown() {
        if (factory != null) {
            factory.close();
        }
    }
}