package pl.com.britenet.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PersonCustomerDto {
    private String userName;
    private String customerName;
    private String customerType;
}
