package pl.com.britenet.dto;

import lombok.Data;
import lombok.NonNull;

@Data
public class PersonCustomerIds {
    @NonNull private Long personId;
    @NonNull private Long customerId;
}
