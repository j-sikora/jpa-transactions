package pl.com.britenet.service;

import pl.com.britenet.dto.PersonCustomerDto;
import pl.com.britenet.dto.PersonCustomerIds;
import pl.com.britenet.entity.Customer;
import pl.com.britenet.entity.Person;
import pl.com.britenet.repository.CustomerRepository;
import pl.com.britenet.repository.PersonRepository;
import pl.com.britenet.utils.transactions.TransactionExecutor;

import javax.persistence.EntityManagerFactory;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class SampleService {
    private EntityManagerFactory entityManagerFactory;
    private PersonRepository personRepository;
    private CustomerRepository customerRepository;

    public SampleService(EntityManagerFactory entityManagerFactory, PersonRepository personRepository, CustomerRepository customerRepository) {
        this.entityManagerFactory = entityManagerFactory;
        this.personRepository = personRepository;
        this.customerRepository = customerRepository;
    }

    public Long createPerson(String name) {
        Person person = TransactionExecutor.create(entityManagerFactory)
                .execute(entityManager ->
                        personRepository.persist(entityManager, new Person(name))
                );
        return person.getId();
    }

    public List<Long> createPersons(List<String> names) {
        return TransactionExecutor.create(entityManagerFactory)
                .execute(entityManager ->
                        names.stream()
                            .map(name -> personRepository.persist(entityManager, new Person(name)))
                            .map(person -> person.getId())
                            .collect(Collectors.toList())
                );
    }

    public PersonCustomerIds savePersonAndCustomer(PersonCustomerDto personCustomerDto) {
        return TransactionExecutor.create(entityManagerFactory)
                .execute(entityManager -> {
                            Person person = personRepository.persist(entityManager, new Person(personCustomerDto.getUserName()));
                            Customer customer = customerRepository.persist(entityManager,
                                    new Customer(personCustomerDto.getCustomerName(), personCustomerDto.getCustomerType()));
                            return new PersonCustomerIds(person.getId(), customer.getId());
                });
    }

    public static void main(String[] args) {
        for (int i = 0; i < 3; i++) {
            System.out.println( UUID.randomUUID());
        }

    }
}
