package pl.com.britenet.repository;

import pl.com.britenet.entity.Person;
import pl.com.britenet.repository.generic.CrudRepository;

public class PersonRepository extends CrudRepository<Person, Long> {
    public PersonRepository() {
        super(Person.class);
    }
}
