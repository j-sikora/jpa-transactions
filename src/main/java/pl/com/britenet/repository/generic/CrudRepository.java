package pl.com.britenet.repository.generic;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.util.List;

public class CrudRepository<E, I> {
    protected final Class<E> entityClass;

    public CrudRepository(Class<E> entityClass) {
        this.entityClass = entityClass;
    }

    public E persist(EntityManager entityManager, E entity) {
        entityManager.persist(entity);
        return entity;
    }

    public E merge(EntityManager entityManager, E entity) {
        return entityManager.merge(entity);
    }

    public void remove(EntityManager entityManager, E entity) {
        entityManager.remove(entity);
    }

    public E findById(EntityManager entityManager,I id) {
        return entityManager.find(entityClass, id);
    }

    public List<E> findAll(EntityManager entityManager) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<E> query =  criteriaBuilder.createQuery(entityClass);
        Root<E> root = query.from(entityClass);
        query.select(root);
        return entityManager.createQuery(query).getResultList();
    }

}
