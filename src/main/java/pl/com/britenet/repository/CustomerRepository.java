package pl.com.britenet.repository;

import pl.com.britenet.entity.Customer;
import pl.com.britenet.repository.generic.CrudRepository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class CustomerRepository extends CrudRepository<Customer, Long> {
    public CustomerRepository() {
        super(Customer.class);
    }

    public List<Customer> findWhereTypeEquals(EntityManager entityManager, String type) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Customer> query =  criteriaBuilder.createQuery(entityClass);
        Root<Customer> root = query.from(entityClass);
        query.select(root);
        query.where(criteriaBuilder.equal(root.get("type"), type));
        return entityManager.createQuery(query).getResultList();
    }
}
