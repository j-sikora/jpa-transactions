create table customer
(
	id bigserial not null,
	name varchar(255) not null,
	type varchar(10) not null,
	uuid uuid not null
);

create unique index customer_id_uindex
	on customer (id);

create unique index customer_name_uindex
	on customer (name);

create unique index customer_uuid_uindex
	on customer (uuid);

alter table customer
	add constraint customer_pk
		primary key (id);


create table person
(
	id bigserial not null,
	name varchar(255) not null,
	uuid uuid not null
);

create unique index person_id_uindex
	on person (id);

create unique index person_name_uindex
	on person (name);

create unique index person_uuid_uindex
	on person (uuid);

alter table person
	add constraint person_pk
		primary key (id);



insert into customer(name,	type, uuid) values('CUSTOMER_1', 'DEFAULT', '6c538e79-8ae4-44de-88fe-6c9bb25e6641');
insert into customer(name,	type, uuid) values('CUSTOMER_2', 'PREMIUM', '218a7915-7e1d-4de1-acb1-0eb4e4a007c7');
insert into customer(name,	type, uuid) values('CUSTOMER_3', 'DEFAULT', 'ff256fba-0d63-4eba-8911-4a81592a6adf');
commit;



