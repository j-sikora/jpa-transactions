package pl.com.britenet.repository;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.testcontainers.junit.jupiter.Testcontainers;
import pl.com.britenet.AbstractContainerTest;
import pl.com.britenet.utils.JPAConfig;

import javax.persistence.EntityManagerFactory;

@Testcontainers
public class CustomerRepositryIT extends AbstractContainerTest {
    private static JPAConfig jpaConfig;
    private static EntityManagerFactory factory;
    private static CustomerRepository customerRepository;

    @BeforeAll
    static void setUp() {
        jpaConfig = new JPAConfig(getProperties());
        factory = jpaConfig.getEntityManagerFactory();
        customerRepository = new CustomerRepository();
    }

    @AfterAll
    static void clean() {
        jpaConfig.shutdown();
    }

    @Test
    public void testFindAllCustomers() {
        final int EXPECTED_CUSTOMER_COUNT = 3;
        Assertions.assertEquals(EXPECTED_CUSTOMER_COUNT,
                customerRepository.findAll(factory.createEntityManager()).size());
    }

    @Test
    public void testFindCustomersWithDefaultType() {
        final int EXPECTED_CUSTOMER_COUNT = 2;
        Assertions.assertEquals(EXPECTED_CUSTOMER_COUNT,
                customerRepository.findWhereTypeEquals(factory.createEntityManager(), "DEFAULT").size());
    }
}
