package pl.com.britenet;

import org.testcontainers.containers.PostgreSQLContainer;

import java.util.HashMap;
import java.util.Map;

public class AbstractContainerTest {
    public static final PostgreSQLContainer container;

    static {
        container = new PostgreSQLContainer();
        container.withInitScript("schema.sql");
        container.start();
    }

    public static Map getProperties() {
        Map<String, Object> properties = new HashMap();
        properties.put("javax.persistence.jdbc.url", container.getJdbcUrl());
        properties.put("javax.persistence.jdbc.driver", container.getDriverClassName());
        properties.put("javax.persistence.jdbc.user", container.getUsername());
        properties.put("javax.persistence.jdbc.password", container.getPassword());
        return properties;
    }
}
