package pl.com.britenet.service;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.testcontainers.junit.jupiter.Testcontainers;
import pl.com.britenet.AbstractContainerTest;
import pl.com.britenet.entity.Person;
import pl.com.britenet.repository.CustomerRepository;
import pl.com.britenet.repository.PersonRepository;
import pl.com.britenet.utils.JPAConfig;
import pl.com.britenet.utils.transactions.TransactionException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Testcontainers
public class SampleServiceIT extends AbstractContainerTest {

    private static JPAConfig jpaConfig;
    private static EntityManagerFactory factory;
    private static PersonRepository personRepository;
    private static CustomerRepository customerRepository;
    private static SampleService sampleService;

    @BeforeAll
    static void setUp() {
        jpaConfig = new JPAConfig(getProperties());
        factory = jpaConfig.getEntityManagerFactory();
        personRepository = new PersonRepository();
        customerRepository = new CustomerRepository();
        sampleService = new SampleService(factory, personRepository, customerRepository);
    }

    @AfterAll
    static void clean() {
        jpaConfig.shutdown();
    }

    @Test
    public void shouldCreatePerson() {
        String name = "TEST1";
        Long personId = sampleService.createPerson(name);
        assertNotNull(personId);
        Person selectedPerson = personRepository.findById(factory.createEntityManager(), personId);
        assertNotNull(selectedPerson);
        assertEquals(personId, selectedPerson.getId());
        assertEquals(name, selectedPerson.getName());
    }

    private long countPersons() {
        EntityManager entityManager = factory.createEntityManager();
        List<Person> persons = entityManager.createQuery("select p from Person p", Person.class).getResultList();
        entityManager.close();
        return persons.size();
    }

    @Test
    public void shouldCreatePersonsWithUniqueNames() {
        final String NAME_PREFIX = "shouldCreatePersonsWithUniqueNames_";
        final long PERSON_COUNT = countPersons();
        String[] names = { NAME_PREFIX + 1, NAME_PREFIX + 2, NAME_PREFIX + 3};
        List<Long> personIds = sampleService.createPersons(Arrays.asList(names));
        assertNotNull(personIds);
        assertEquals(names.length, personIds.size());
        System.out.println(countPersons());
        assertEquals(PERSON_COUNT + personIds.size(), countPersons());
    }

    @Test
    public void shouldThrowsTransactionalExceptionPersonNameShouldBeUnique() {
        final String NAME_PREFIX = "shouldThrowsTransactionalExceptionPersonNameShouldBeUnique_";
        final long PERSON_COUNT = countPersons();
        String[] names = { NAME_PREFIX + 1, NAME_PREFIX + 2, NAME_PREFIX + 3, NAME_PREFIX + 2, NAME_PREFIX + 4};
        TransactionException transactionException = assertThrows(TransactionException.class, () -> sampleService.createPersons(Arrays.asList(names)));
        assertTrue(transactionException.getCause().getMessage().contains("ConstraintViolationException"));
        assertEquals(PERSON_COUNT, countPersons());
    }
}
